import hero from "./hero.jpg"

function MainPage() {
  return (
    <div class="container text-center">
     <div className="px-4 py-5 my-5 text-center">
       <h1 className="display-5 fw-bold">Automate</h1>
       <div className="col-lg-6 mx-auto">
         <p className="lead mb-4">
           The premiere solution for automobile dealership
           management!
         </p>
       </div>
       <img
         src={hero} alt="hero"
         style={{
           height: "800px"
         }}/>
     </div>
    </div>
  );
}

export default MainPage;
